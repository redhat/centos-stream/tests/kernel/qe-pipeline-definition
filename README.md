# Kernel QE CI pipelines

## Pipelines

These are pipelines owned by kernel-qe team to run tests for kernel and
userspace packages.

Each pipeline trigger can contain multiple jobs with tests from different
person/team. All jobs from the same trigger should report to same
[DataWarehouse checkout](https://datawarehouse.cki-project.org)

## Directory layout

| directory                    | purpose                                                                         |
|------------------------------|---------------------------------------------------------------------------------|
| [definitions/](definitions/) | test definition YML files that define what tests to run when (parent pipelines) |
| [runners/](runners/)         | runner YML files that interface with the test orchestrators (child pipelines)   |
| [scripts/](scripts/)         | shell scripts implementing the actual pipeline functionality                    |
| [spec/](spec/)               | [ShellSpec] tests for the shell scripts                                         |
| [internal/](internal/)       | internal machinery                                                              |

## Onboard new tests to a pipeline

1. Make sure there is a test plan template for the test under
   [definitions/plan-templates](definitions/plan-templates).
   If your team does not have a directory here yet, please
   create one.

   Example using **wow** plugin to generate beaker jobs:

   ```yaml
   .general_security:
     variables:
       TEST_JOB_NAME: general_security                                                         # copy your test plan name
       JOB_OWNER: "bgoncalv"                                                                   # bkr job submitted as this user
                                                                                               # also used for slack notification if JOB_NOTIFY is not set)
       TEST_PACKAGE_NAME_ARCHES: "kernel-aarch64 kernel-ppc64le kernel-s390x kernel-x86_64"    # kernel variant name and architecture(s) to be used
       WOW_WHITEBOARD: "Kernel Security Regression tests ${DW_CHECKOUT}"                       # beaker whiteboard
       WOW_TASKFILE: "${KERNEL_QE_CI_INTERNAL_URL}/general/security/tasks/SECURITY"            # wow taskfile
   ```

   Example using **job-submitter** tool to generate beaker jobs:

   ```yaml
   .general_perf:
     variables:
       TEST_JOB_NAME: general_perf                                                             # copy your test plan name
       JOB_OWNER: "bgoncalv"                                                                   # bkr job submitted as this user
                                                                                               # also used for slack notification if JOB_NOTIFY is not set)
       TEST_PACKAGE_NAME_ARCHES: "kernel-x86_64"                                               # kernel variant name and architecture(s) to be used
       TEST_PLAN_REPO: ""                                                                      # test plan repo (related to job-submitter root directory)
       TEST_PLAN_NAME: "ci"                                                                    # job-submitter test plan name
       JOB_SUBMITTER_PARMS: "-f PERF --short-whiteboard --job-owner=bgoncalv"                  # parameters to be passed to job-submitter
   ```

   Example using **zsubmit** tool:

   ```yaml
   .sustaining:
     variables:
       TEST_JOB_NAME: sustaining                                                               # copy your test plan name
       TEST_PACKAGE_NAME_ARCHES: "kernel-x86_64"                                               # kernel variant name and architecture(s) to be used
       ZSUBMIT_TIER: 'KT1 KT2 REG'                                                             # zsubmit tier(s)
       ZSUBMIT_PARAMS: '--noduplicate'                                                         # additional parameters to zsubmit
       JOB_NOTIFY: "bgoncalv"                                                                  # user to be notified on slack channel
   ```

   Example of storage test plan template using **Testing Farm**:

   ```yaml
   ---
   .storage_iscsi:
     variables:
       TEST_JOB_NAME: storage_iscsi                                                            # copy your test plan name
       TFT_PLAN_URL: "https://gitlab.com/redhat/centos-stream/tests/kernel/test-plans.git"     # repository with test plan
       TFT_PLAN: "iscsi/local"                                                                 # name of the test plan
       TFT_HARDWARE: ""                                                                        # (Optional) testing farm hardware requirements (semi-colon seperated).
       TFT_PARAMS: ""                                                                          # (Optional) testing farm additional parameters (semi-colon seperated).
       TEST_PACKAGE_NAME_ARCHES: "kernel-aarch64 kernel-x86_64"                                # kernel variant name and architecture(s) to be used
       JOB_NOTIFY: "bgoncalv"                                                                  # user to be notified on slack channel
   ```

   Example of storage test plan template using **stbkr** to generate the beaker jobs:

   ```yaml
   .storage_dm_crypt:
     variables:
       TEST_JOB_NAME: storage_dm_crypt                                                         # copy your test plan name
       CONFIG: "dm-crypt.conf"                                                                 # stbkr config file
       STBKR_GROUP: "common"                                                                   # sbkt group name
       JOB_OWNER: "bgoncalv"                                                                   # bkr job submitted as this user
                                                                                               # also used for slack notification if JOB_NOTIFY is not set)
       TEST_PACKAGE_NAME_ARCHES: "kernel-x86_64 kernel-debug-x86_64"                           # kernel variant name and architecture(s) to be used
   ```

2. Under [definitions](definitions/), select the right RHEL release and add the
   new job to your sub-team under `plan-jobs` directory:

   ```yaml
   ---
   include:
     - definitions/plan-templates/rts/rt.yml                                       # include your team's test plan template

   rt_tier2_ystream:                                                               # from your team's test template, select the test plan to run
     extends: [.9_5-bkr-job-submitter-runner, .rt_tier2_ystream, .kernel_matrix]   # extend your test plan, runner to submit jobs, and test matrix
   ```

   1. If there your sub-team doesn't have a file yet, please create a new one.
   2. When adding a new job, you need to extend it using a submitter runner job
      template for the RHEL release and the test job from the test plan template:

      1. Select the submitter runner based on the tool your test job requires
         to create the test job.
      2. If the tool is not supported it, please open an issue requesting
         support for it.

## Triggers

### Schedulers

  - The sub-team job should be included to the scheduler trigger jobs.
    - They are under `schedules` directory and named like `weekly.yaml` or
      `biweekly-*.yaml`.
  - If your job needs to run on different cadence or the scheduler file is
    missing for the release, please open an issue.
  - For userspace candidate jobs, please ensure the pipeline only runs the
    sub-team job when the package name matches what was expected.

      1. For example with [definitions/rhel9/9.6/schedules/userspace-candidate.yml]:

         ```yaml
         include:
           - local: definitions/rhel9/9.6/plan-jobs/rt-userspace.yml
         ```

      2. [definitions/rhel9/9.6/plan-jobs/rt-userspace.yml] has a rule for each pipeline
         matching `$_TEST_SOURCE_PACKAGE_NVR`:

         ```yaml
         rt_tier0_realtime_setup:
           extends: [.9_6-bkr-job-submitter-runner, .rt_tier0_realtime_setup, .overrides]
           rules:
             - if: $_TEST_SOURCE_PACKAGE_NVR =~ /realtime-setup.*/
         ```

      3. Furthermore, [definitions/umb-triggers.yml] needs to include the package name
         to listen to in the `USERSPACE_CANDIDATE_PKG_LIST` regex.

[definitions/rhel9/9.5/schedules/userspace-candidate.yml]: definitions/rhel9/9.5/schedules/userspace-candidate.yml
[definitions/rhel9/9.5/plan-jobs/rt-userspace.yml]: definitions/rhel9/9.5/plan-jobs/rt-userspace.yml
[definitions/umb-triggers.yml]: definitions/umb-triggers.yml

### UMB triggers

1. Add a new pipeline job to the [UMB triggers](definitions/umb-triggers.yml)

Currently supported topics:

- VirtualTopic.eng.brew.build.tag
- VirtualTopic.eng.cki.ready_for_test
- VirtualTopic.eng.errata.activity.status

If you need to trigger on a different topic, please [open an issue].

## Create a new runner

The child pipelines do the actual work of submitting a job to the test
orchestrator, waiting for it to finish, triaging results etc. They are defined
in the [runners/](runners/) directory.

To create a new runner:

1. copy one of the existing `runners/runner-something.yml` files to a new name
2. modify it as needed
3. add any needed scripts to [scripts/](scripts/)
4. add tests for the scripts via [ShellSpec] in [spec/](spec/)
5. add a trigger job to [runners/trigger-jobs.yml](runners/trigger-jobs.yml)

[ShellSpec]: https://shellspec.info/
[open an issue]: https://gitlab.com/redhat/centos-stream/tests/kernel/qe-pipeline-definition/-/issues/new

## Publishing to OSCI

To publish a pipeline's test results to OSCI, you must define the following `REPORT_OSCI` variable:

```yaml
variables:
  REPORT_OSCI: "1"     # To confirm OSCI publishing is enabled
```

This will use the default values in the message:
-  `category: "functional"`
-  `namespace: "${package_name}-${arch}"`
-  `type: "tier1"`

The default can be changed by setting additional variables:
- `OSCI_TEST_CATEGORY`
- `OSCI_TEST_NAMESPACE`
- `OSCI_TEST_TYPE`

## Publishing to Polarion

To publish a pipeline's test results to Polarion, you must define the following variables:

```yaml
variables:
  REPORT_POLARION: "1"                              # To confirm Polarion publishing is enabled
  POLARION_OWNER: "cyin"                            # An assignee/owner for the Polarion test run
  POLARION_CONFIG_MAP_FILE: Configs/rhel-rt.json    # A FeedPolarion config file mapping test case names to Polarion IDs
```

Note: Publishing will only be enabled for the latest versions of each RHEL major release.

## RHEL Stream Transitions

Pipeline definitions are organized by RHEL releases.  An active RHEL release in development is known as a "Y Stream," while RHEL versions that have already been released but are under Red Hat support are known as "Z Streams."

There are stages in the RHEL development lifecycle where changes must be made to this repository.  For these examples, we will assume the release number currently in development is `n`, the previous release was `n-1`, and next active Y Stream release is `n+1`.

- When `n+1` begins development:
  - QE maintainer(s) of `n+1` should copy all `plan-jobs` and `schedules` for release `n` to `n+1`, updating numerical release number references as needed 
  - QE maintainer(s) of `n+1` should update [umb-triggers.yml](definitions/umb-triggers.yml) to run any required build/candidate testing
  - CI admins will copy all schedules for `n` to additionally run for `n+1`.  For example, a weekly pipeline trigger for `n` will now also trigger for `n+1`
- When `n` GA's and transitions to Z Stream:
  - QE maintainer(s) for `n` will need to clean up `plan-jobs` and `schedules` to remove any unnecessary Y Stream triggers while simultaneously adding any newly required Z Stream triggers.  For example:
    - The QE maintainer may begin to copy `n-1` jobs/schedules into `n`, adjusting definitions as needed
    - The QE maintainer would also update [umb-triggers.yml](definitions/umb-triggers.yml) to remove any Y Stream triggers that are no longer required and define new Z Stream triggers
    - Some Tier-1 testing may only need to run for Y Stream releases and such jobs may be removed following `n`'s GA
      - Component gating tests however may need to remain active for `n` to avoid blocking OSCI gating
    - The QE maintainer will update the `common.yml` for `n` to ensure appropriate runners are defined for Z Stream jobs, such as `bkr-zsubmit-submitter-runner`
  - CI admins will delete all schedules for `n`.  For example, there will no longer be a weekly pipeline trigger for `n`.
- When `n` reaches EOL:
  - The QE maintainer for stream `n` shall delete the definition directory for `n` and remove any triggers or references to `n` that remain in the repository

## FAQ

### How do I test specific pipelines in a merge request?

Each merge request will trigger pipelines to run `schemalint`, `shellcheck`, `shellspec`, `yamllint`, and `integration-test`.  These are defined in [.gitlab-ci.yml](.gitlab-ci.yml).

The `integration-test` will run whatever is defined in `QE_PIPELINE_DEFINITION_PIPELINE_FILENAME`, which by default should be [definitions/integration-tests.yml](definitions/integration-tests.yml).  However, you may *temporarily* change this to test a different pipeline in your merge request to verify the changes will work.

To do so:

1. Under `integration-test` in [.gitlab-ci.yml](.gitlab-ci.yml):
  - Point `QE_PIPELINE_DEFINITION_PIPELINE_FILENAME` to a pipeline schedule to test, e.g. [definitions/rhel9/9.0/schedules/kernel-candidate.yml](definitions/rhel9/9.0/schedules/kernel-candidate.yml)
  - you **must** add variable `_DRY_RUN: '1'`, to ensure that no test jobs are submitted
  - Add additional variables needed for that pipeline, such as `_TEST_SOURCE_PACKAGE_NVR: "kernel-5.14.0-70.117.1.el9_0"`

When you are satisfied with the pipeline results, simply revert your changes to [.gitlab-ci.yml](.gitlab-ci.yml) and the `QE_PIPELINE_DEFINITION_PIPELINE_FILENAME` file before requesting review for merge.

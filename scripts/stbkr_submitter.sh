#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

. scripts/bkr_common.sh

set -x
# don't fail pipeline if a command fails
set +e
git clone --depth 1 "${STBKR_TESTS_CONF_REPO:?}" kernel
pushd kernel/storage/misc/toolbox || exit 1
# remove the / that are passed with TEST_PACKAGE_NAME_ARCH
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
stbkr_params=()
if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}
    # From the help:
    #   --kernel=$nvr
    #       Using distribution/kernelinstall to install kernel.
    #       Please use NVR like 'kernel-debug-2.6.32-405.el6.kpq1'.
    #   --pkg=$nvr
    #       Using distribution/pkginstall to install certain package.
    #       Please use NVR like 'autobkr-0.1-11.el6eso'.
    if [[ "${TEST_PACKAGE_NVR}" =~ "kernel" ]]; then
        stbkr_params+=(--kernel "${TEST_PACKAGE_NVR}")
        if [[ "${TEST_PACKAGE_NVR}" =~ "kernel-rt" ]]; then
            # when running kernel-rt it should add efi=runtime to kernel options
            stbkr_params+=(--kernel_ops 'efi=runtime')
        fi
    else
        stbkr_params+=(--pkg "${TEST_PACKAGE_NVR}")
    fi
fi

if [[ "${TEST_PACKAGE_NAME}" =~ "kernel" ]]; then
    if [[ "${TEST_PACKAGE_NAME}" == "kernel" ]]; then
        stbkr_params+=(--default-kernel 1)
        stbkr_params+=(--debug-kernel 0)
        stbkr_params+=(--rt-kernel 0)
        stbkr_params+=(--64k-kernel 0)
    elif [[ "${TEST_PACKAGE_NAME}" == "kernel-debug" ]]; then
        stbkr_params+=(--default-kernel 0)
        stbkr_params+=(--debug-kernel 1)
        stbkr_params+=(--rt-kernel 0)
        stbkr_params+=(--64k-kernel 0)
    elif [[ "${TEST_PACKAGE_NAME}" == "kernel-rt" ]]; then
        stbkr_params+=(--default-kernel 0)
        stbkr_params+=(--debug-kernel 0)
        stbkr_params+=(--rt-kernel 1)
        stbkr_params+=(--64k-kernel 0)
        # when running kernel-rt it should add efi=runtime to kernel options
        stbkr_kernel_ops_params+=(efi=runtime)
        stbkr_params+=(--kernel_ops 'efi=runtime')
    elif [[ "${TEST_PACKAGE_NAME}" == "kernel-64k" ]]; then
        stbkr_params+=(--default-kernel 0)
        stbkr_params+=(--debug-kernel 0)
        stbkr_params+=(--rt-kernel 0)
        stbkr_params+=(--64k-kernel 1)
    else
        echo "FAIL: unsupported kernel package ${TEST_PACKAGE_NAME}"
        exit 1
    fi
else
    stbkr_params+=(--default-kernel 1)
    stbkr_params+=(--debug-kernel 0)
    stbkr_params+=(--rt-kernel 0)
    stbkr_params+=(--64k-kernel 0)
fi

if [[ -n "${TEST_COMPOSE_TAGS:-}" ]]; then
    # shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
    if [[ $(wc -w <<< "${TEST_COMPOSE_TAGS}") -gt 1 ]]; then
        echo "FAIL: TEST_COMPOSE_TAGS (${TEST_COMPOSE_TAGS}) has to be 1 word"
        exit 1
    fi
    stbkr_params+=( --dist_tag "${TEST_COMPOSE_TAGS}")
fi

if [[ -n "${STBKR_GROUP:-}" ]]; then
    stbkr_params+=(--group "${STBKR_GROUP}")
fi

if [[ "${DRY_RUN:-}" == "1" ]]; then
    # there is no dry run mode, but --savefile doesn't submit a beaker job, just creates job.xml
    stbkr_params+=(--savefile job.xml)
fi

if ! stbkr "${stbkr_params[@]}" -r "${TEST_COMPOSE:?}" -c test_conf/"${CONFIG:?}" \
         -a "${TEST_ARCH}" --job-owner "${JOB_OWNER:?}" > bkr_job.txt; then
    echo "FAIL: running stbkr command."
    exit 1
fi
if [[ "${DRY_RUN:-}" == "1" ]]; then
    cat job.xml
    echo "INFO: running as dry run, no job is submitted."
    echo "['J:123456']" > bkr_job.txt
fi

BEAKER_JOBIDS=$(sed -e "s/.*\['\(.*\)'\]/\1/g" < bkr_job.txt)
popd || exit 1
if [[ -z "${BEAKER_JOBIDS}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi

wait_jobs

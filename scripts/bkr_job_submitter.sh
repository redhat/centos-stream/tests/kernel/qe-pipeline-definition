#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

. scripts/bkr_common.sh

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')

JOB_SUBMITTER_PARMS="${JOB_SUBMITTER_PARMS} --arch ${TEST_ARCH}"

if [[ -n "${TEST_PLAN_REPO:-}" ]]; then
    git clone --depth 1 "${TEST_PLAN_REPO}" test-plan
    cp -r test-plan/"${TEST_PLAN_PATH:?}" ~/
    rm -rf test-plan
fi

# job submitter doesn't require a compose, if a compose is not set it will
# select a compose based on package nvr.
if [[ -n "${TEST_COMPOSE:-}" ]]; then
    bkr_get_compose
    BUILD="${BKR_DISCOVERED_COMPOSE}"
    export BUILD
fi

if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_SOURCE_PACKAGE_NVR=$(find_compose_pkg -c "${BUILD}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_SOURCE_PACKAGE_NAME} on compose ${BUILD}"
        exit 1
    fi
fi

TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}

# if using TEST_PACKAGE_IS_NOARCH the TEST_PACKAGE_NVR should get .noarch when passing to JobSubmitter
# but we should not update TEST_PACKAGE_NVR for the rest of pipeline execution.
pkg_nvr=${TEST_PACKAGE_NVR}
if [[ "${TEST_PACKAGE_IS_NOARCH:-}" == "true" ]]; then
    # some userspace packages are built as noarch which must be explicitly specified
    # when passed to JobSubmitter
    pkg_nvr="${TEST_PACKAGE_NVR}.noarch"
fi
JOB_SUBMITTER_PARMS="${JOB_SUBMITTER_PARMS} --nvr ${pkg_nvr}"

# if triggered by a CKI MR build, export BUILD_REPO variable for the pkg under test
# note: JobSubmitter also expects BUILD_MR_URL, which is already set in umb-triggers.yml
if [[ -n "${BUILD_REPOS:-}" ]]; then
    # shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
    readarray -t build_arr < <(jq -c '.[]' <<< "${BUILD_REPOS}")
    for build in "${build_arr[@]}"; do
        build_arch="$(echo "${build}" | jq -r '.arch')"
        build_name="$(echo "${build}" | jq -r '.name')"
        if [[ "${TEST_ARCH:-}" == "${build_arch:-}" && "${TEST_PACKAGE_NAME:-}" == "${build_name:-}" ]]; then
            BUILD_REPO="$(echo "${build}" | jq -r '.url')"
            export BUILD_REPO
            break
        fi
    done
    if [[ -z "${BUILD_REPO:-}" ]]; then
        echo "FAIL: could not find ${TEST_PACKAGE_NAME}-${TEST_ARCH} build repo from ${BUILD_REPOS}"
        exit 1
    fi
fi

# allow JOB_SUBMITTER_PARMS to contain command line execution
JOB_SUBMITTER_PARMS=$(eval echo "${JOB_SUBMITTER_PARMS}")

if [[ "${DRY_RUN:-}" == "1" ]]; then
    JOB_SUBMITTER_PARMS="${JOB_SUBMITTER_PARMS} --dry-run"
fi

# we want globbing on TEST_PLAN_NAME and JOB_SUBMITTER_PARMS
# shellcheck disable=SC2086 # Double quote to prevent globbing
JobSubmitter.sh ${TEST_PLAN_NAME:?} ${JOB_SUBMITTER_PARMS} 2>&1 | tee bkr_job.txt

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running as dry run, no job is created."
    echo 'TJ#123456' | tee bkr_job.txt
fi

BEAKER_JOBIDS=$(grep -Eo 'TJ#[[:digit:]]+$' bkr_job.txt | sed 's/TJ#/J:/' | tr '\n' ' ')


if [[ -z "${BEAKER_JOBIDS}" ]]; then
    echo "INFO: No job submitted to beaker."
    exit 0
fi

wait_jobs

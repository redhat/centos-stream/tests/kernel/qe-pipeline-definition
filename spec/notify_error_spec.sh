#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'notify_error'
    cleanup() {
        rm -f slack_message.txt
    }
    After 'cleanup'

    export KOJI_SERVER="test-koji"
    export CI_PIPELINE_URL="pipeline.test/"
    export CI_PIPELINE_ID="123456"

    Mock send_slack_notification
    End

    Mock result2osci
        echo "result2osci ${*}"
    End

    It 'can notify'
        When run script scripts/notify_error.sh
        The status should be success
        The stdout should include "@bgoncalv please review"
        The stderr should include "send_slack_notification --message-file slack_message.txt"
    End

    It 'can send message to OSCI'
        export REPORT_OSCI=1
        export TEST_ARCH="x86_64"
        export TEST_PACKAGE_NAME="kernel"
        export TEST_SOURCE_PACKAGE_NVR="kernel-5.14.0-496.el9"
        When run script scripts/notify_error.sh
        The status should be success
        The stdout should include "@bgoncalv please review"
        The stderr should include "send_slack_notification --message-file slack_message.txt"
        The stderr should include "result2osci --source-nvr kernel-5.14.0-496.el9 --certificate /tmp/umb_certificate.pem --pipeline-id 123456 --log-url pipeline.test/ --test-status error --run-url pipeline.test/ --test-namespace kernel-qe.kernel-ci.kernel-x86_64"
    End
End
